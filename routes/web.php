<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@register');
Route::post('/regist', 'AuthController@regist');
Route::get('/data-tables', 'IndexController@dataTables');

// Route::get('/layout', function(){
//     return view('layout.master');
// });

// CRUD

Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}','CastController@update' );
Route::delete('/cast/{cast_id}', 'CastController@destroy');


