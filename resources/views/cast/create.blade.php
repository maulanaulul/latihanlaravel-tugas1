@extends('layout.master')
@section('title')
Halaman Tambah Cast
@endsection
@section('content')
<form method="POST" action="/cast">
    @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Nama</label>
    <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    

  </div>

  @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label for="exampleInputEmail1">Umur</label>
    <input type="number" name="umur" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>

  @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
  <div class="form-group">
    <label for="exampleInputEmail1">Bio</label>
    <input type="text" name="bio" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>

  @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
 
  <button type="submit" class="btn btn-primary">Create</button>
</form>

@endsection