@extends('layout.master')
@section('title')
Halaman List Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary">Tambah Cast</a>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse($cast as $key => $item) 
    <tr>
      <th scope="row">{{$key + 1}}</th>
      <td>{{$item->nama}}</td>
      <td>{{$item->umur}}</td>
      <td>{{$item->bio}}</td>
      <td> 
        <form action="/cast/{{$item->id}}" method="POST">
        <a href="/cast/{{$item->id}}" class="btn btn-primary">Detail</a>
        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
            @csrf 
            @method('delete')
            <input type="submit" value="delete" class="btn btn-danger ">
        </form>
    </td>
    </tr>
    @empty
        <h4>Data Cast tidak ada</h4>
    @endforelse
    
  </tbody>
</table>
@endsection