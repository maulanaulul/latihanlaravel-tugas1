@extends('layout.master')
@section('title')
Halaman Edit Cast
@endsection
@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
  <div class="form-group">
    <label for="exampleInputEmail1">Nama</label>
    <input type="text" value="{{$cast->nama}}" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    

  </div>

  @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label for="exampleInputEmail1">Umur</label>
    <input type="number" value="{{$cast->umur}}" name="umur" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>

  @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
  <div class="form-group">
    <label for="exampleInputEmail1">Bio</label>
    <input type="text" name="bio" value="{{$cast->bio}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>

  @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
 
  <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection