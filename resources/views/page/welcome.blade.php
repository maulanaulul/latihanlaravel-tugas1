
@extends('layout.master')
@section('title')
Welcome Page
@endsection
@section('content')
<h1>Welcome {{$firstname}} {{$lastname}}</h1>
<h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
<br><br>
<strong><h2>Biodata</h2></strong> <br>
<p><strong>Fullname : </strong> {{$firstname}} {{$lastname}} </p>
<p><strong>Gender : </strong> {{$gender}} </p>
<p><strong>Nationality : </strong> {{$nationality}} </p>
<p><strong>Language : </strong> {{$bindo}} {{$beng}} </p>
<p><strong>Bio : </strong> {{$bio}} </p>


@endsection