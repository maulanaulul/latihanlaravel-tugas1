

@extends('layout.master')
@section('title')
Halaman Register
@endsection
@section('content')
<form action="/regist" method="POST">
        @csrf

        <label for="">First Name</label> <br>
        <input type="text" name="firstName" required>
        <br> <br>
        <label for="">Last Name</label>
        <br>
        <input type="text" name="lastName" required>
        <br><br>
        <label for="">Gender</label>
        <br> 
        <input type="radio" value="Male" name="gender" > <label for="">Male</label>
        <br>
        <input type="radio" value="Female" name="gender">
        <label for="">Female</label>
        <br><br>
        <label for="">Nationality</label>
        <br>
        <select name="nationality" id="">
            <option value="indonesia">Indonesia</option>
            <option value="English">English</option>
        </select>
        <br> <br>
        <label for="">Language Spoken</label>
        <br>
        <input type="checkbox" name="indonesia" value="Bahasa Indonesia" id="">
        <label for="">Bahasa Indonesia</label>
        <br>
        <input type="checkbox" name="english" value="English" id="">
        <label for="">English</label>
        <br>
        <input type="checkbox" name="other" id="">
        <label for="">other</label>

        <br><br>
        <label for="">Bio</label>
        <br><br>
        <textarea name="bio" id="" cols="30" rows="10" required></textarea>
        <br>
        <button type="submit">Sign up</button>
    </form>

@endsection