<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function regist(Request $request)
    {
        // dd($request->all());
        $firstname = $request['firstName'];
        $lastname = $request['lastName'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $bio = $request['bio'];
        $bindo = $request['indonesia'];
        $beng = $request['english'];


        return view('page.welcome', compact("firstname", "lastname", "gender", "nationality", "bio", "bindo", "beng"));
    }
}
